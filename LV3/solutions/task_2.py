import pandas as pd
from matplotlib import pyplot as plt
import os

print(os.getcwd())
df=pd.read_csv('../resources/mtcars.csv')
with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
    print(df)

print('part 1')
df.groupby(df['cyl'])['mpg'].mean().plot.bar()
plt.show()

print('part 2')
df.boxplot('wt','cyl')
plt.show()

print('part 3')
df.boxplot('mpg','am') #automatic transmission cars have a lower mpg
plt.show()

print('part 4')
df.plot.scatter('qsec','hp', c='am',colormap='viridis')
plt.show()