import os
import pandas as pd
import numpy as np

print(os.getcwd())
df=pd.read_csv('../resources/mtcars.csv')
with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
    print(df)

print("part 1")
print(df.sort_values('mpg').head())

print("part 2")
print(df[df['cyl']==8].sort_values('mpg',ascending=False).head(3))

print("part 3")
print(df[df['cyl']==6]['mpg'].mean())

print("part 4")
print(df[(df['cyl']==4) & (df['wt']>2.000) & (df['wt']<2.200)])

print("part 5")
print(len(df[df['am']==0]))
print(len(df[df['am']==1]))

print("part 6")
print(len( df[(df['am']==0) & (df['hp']>100)] ))

print("part 7")
print('expressed in tons')
print(df['wt'].apply(lambda x: x*0.4535))