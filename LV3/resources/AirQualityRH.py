import urllib.request
from calendar import monthrange
import matplotlib.pyplot as plt
import pandas as pd
import xml.etree.ElementTree as ET

# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=5&vrijemeOd=01.01.2017&vrijemeDo=31.12.2017'

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)
df.plot(y='mjerenje', x='vrijeme');
plt.show()

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek
print(df.describe())

print('part 2')
print(df.sort_values('mjerenje',ascending=False)['vrijeme'].head(3))

print('part 3')
day_counts=[]
for month_num in range(1,13):
    day_counts.append(monthrange(2017,month_num)[1])
diff=day_counts-df.groupby('month')['mjerenje'].count()
diff.plot.bar()
plt.show()

print('part 4')
plt.boxplot(df[df['month']==1]['mjerenje'])
plt.boxplot(df[df['month']==7]['mjerenje'])
plt.show()

print(df['dayOfweek'])

print('part 5')
df['is_weekday']=(df['dayOfweek']<5)
df.boxplot('mjerenje','is_weekday')
plt.show()
