# Napišite program koji od korisnika zahtijeva unos imena tekstualne datoteke. Program nakon toga treba tražiti linije
# oblika:
# X-DSPAM-Confidence: <neki_broj>
# koje predstavljaju pouzdanost korištenog spam filtra. Potrebno je izračunati srednju vrijednost pouzdanosti. Koristite
# datoteke mbox.txt i mbox-short.txt
# Primjer
# Ime datoteke: mbox.txt
# Average X-DSPAM-Confidence: 0.894128046745
# Ime datoteke: mbox-short.txt
# Average X-DSPAM-Confidence: 0.750718518519

sum = 0
count = 0

fname = input()
try:
    with open(fname) as file:
        for line in file:
            if line.startswith('X-DSPAM-Confidence: '):
                 count += 1
                 sum += float(line.split()[-1])
    print('avg: ', sum / count)
except FileNotFoundError:
    print('File not found')
