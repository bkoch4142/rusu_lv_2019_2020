# Napišite Python skriptu koja će učitati tekstualnu datoteku te iz redova koji počinju s „From“ izdvojiti mail adresu te ju
# spremiti u listu. Nadalje potrebno je napraviti dictionary koji sadrži hostname (dio emaila iza znaka @) te koliko puta se
# pojavljuje svaki hostname u učitanoj datoteci. Koristite datoteku mbox-short.txt. Na ekran ispišite samo nekoliko
# email adresa te nekoliko zapisa u dictionary-u.

emails = []
domains = dict()

fname = input('')
try:
    with open(fname) as file:
        for line in file:
            if line.startswith('From: '):
                 emails.append(line.split()[-1])
                 domain = emails[-1].split('@')[-1]
                 if domain not in domains:
                     domains[domain] = 1
                 else:
                     domains[domain] += 1
except FileNotFoundError:
    print('File not found')
