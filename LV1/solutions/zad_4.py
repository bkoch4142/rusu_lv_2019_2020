# Napišite program koji od korisnika zahtijeva unos brojeva u beskonačnoj petlji sve dok korisnik ne upiše „Done“ (bez
# navodnika). Nakon toga potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
# vrijednost. Osigurajte program od krivog unosa (npr. slovo umjesto brojke) na način da program zanemari taj unos i
# ispiše odgovarajuću poruku.

nums=[]
while True:
    inpt=input()
    if inpt == "Done":
        break
    else:
        try:
            inpt=int(inpt)
            nums.append(inpt)
        except:
            print('invalid input')

print(sum(nums)/len(nums))

