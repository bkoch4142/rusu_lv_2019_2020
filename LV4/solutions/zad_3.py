import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

def non_func(x):
 y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
 return y

def add_noise(y):
 np.random.seed(14)
 varNoise = np.max(y) - np.min(y)
 y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
 return y_noisy

x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]
x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]
xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]
xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

lr=0.01
epochs=10000
theta0=1
theta1=1
def h(theta0, theta1, xtrain):
    return theta0+theta1*xtrain

for epoch in range(epochs):
    cost=1/(2*len(xtrain))*np.sum((h(theta0,theta1,xtrain)-ytrain)**2)
    theta0_grad=1/(len(xtrain))*np.sum(h(theta0,theta1,xtrain)-ytrain)
    theta1_grad=1/(len(xtrain))*(np.transpose((theta0+xtrain*theta1)-ytrain)@xtrain)
    theta0=theta0-lr*(theta0_grad)
    theta1=theta1-lr*(theta1_grad)
    print(cost, theta0_grad, theta1_grad, theta0, theta1)

print(theta0,theta1)