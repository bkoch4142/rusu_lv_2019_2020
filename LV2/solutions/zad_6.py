'''
Na temelju primjera 2.8. učitajte sliku 'tiger.png'. Manipulacijom odgovarajuće numpy matrice pokušajte posvijetliti
sliku (povećati brightness).
'''

import cv2

img=cv2.imread('LV2/resources/tiger.png')
img=img+50
cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
print(img.shape)
