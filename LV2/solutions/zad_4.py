'''
Simulirajte 100 bacanja igraće kocke (kocka s brojevima 1 do 6). Pomoću histograma prikažite rezultat ovih bacanja.
'''

import numpy  as np
import matplotlib.pyplot as plt

dice_results=np.random.randint(1,7, size=100)
print(dice_results)

plt.hist(dice_results, bins=6)
plt.show()