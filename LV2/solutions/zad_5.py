'''
U direktoriju rusu_lv_2019_20/LV2/resources nalazi se datoteka mtcars.csv koja sadrži različita
mjerenja provedena na 32 automobila (modeli 1973-74). Prikažite ovisnost potrošnje automobila (mpg) o konjskim
snagama (hp). Na istom grafu prikažite i informaciju o težini pojedinog vozila. Ispišite minimalne, maksimalne i
srednje vrijednosti potrošnje automobila.
'''

import csv
import matplotlib.pyplot as plt

mtcars_path='LV2/resources/mtcars.csv'
hp=[]
mpg=[]
wt=[]

with open(mtcars_path, 'r') as f:
    reader=csv.reader(f)

    next(reader)
    for row in reader:
        hp.append(float(row[4]))
        mpg.append(float(row[1]))
        wt.append(float(row[6]))

hp_mpg_zip=zip(hp,mpg)
hp_mng_zip_sorted= sorted(hp_mpg_zip, key = lambda x: x[0])

print(min(mpg))
print(max(mpg))
print(len(mpg)/len(mpg))

print(wt)

plt.scatter(hp, mpg, s=[i*20 for i in wt])
plt.show()
