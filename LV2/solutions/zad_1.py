'''
Napišite Python skriptu koja će učitati tekstualnu datoteku, pronaći valjane email adrese te izdvojiti samo prvi dio
adrese (dio ispred znaka @). Koristite odgovarajući regularni izraz. Koristite datoteku mbox-short.txt. Ispišite rezultat.
'''

import re
import os

text_path="LV2/resources/mbox-short.txt"
matches=None
with open(text_path,'r') as f:
    text=f.read()
    pattern=r'(\w+)@'
    matches=re.findall(pattern,text)

print(matches)