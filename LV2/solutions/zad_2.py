'''
Na temelju rješenja prethodnog zadatka izdvojite prvi dio adrese (dio ispred @ znaka) samo ako:
1. sadrži najmanje jedno slovo a
2. sadrži točno jedno slovo a
3. ne sadrži slovo a
4. sadrži jedan ili više numeričkih znakova (0 – 9)
5. sadrži samo mala slova (a-z)
'''

import re
setting=3

text_path="LV2/resources/mbox-short.txt"
matches=None
with open(text_path,'r') as f:
    text=f.read()
    if setting==1:
        pattern=r'(\w*a+\w*)@'
    if setting==2:
        pattern=r'([b-z0-9]*a[b-z0-9]*)\@'
    if setting==3:
        pattern=r'([b-z0-9]+)\@'
    if setting==4:
        pattern=r'(\w*[0-9]+\w*)@'
    if setting==5:
        pattern=r'([a-z]+)@'


    matches=re.findall(pattern,text)

print(matches)