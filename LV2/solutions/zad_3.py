'''
Napravite jedan vektor proizvoljne dužine koji sadrži slučajno generirane cjelobrojne vrijednosti 0 ili 1. Neka 1
označava mušku osobu, a 0 žensku osobu. Napravite drugi vektor koji sadrži visine osoba koje se dobiju uzorkovanjem
odgovarajuće distribucije. U slučaju muških osoba neka je to Gaussova distribucija sa srednjom vrijednošću 180 cm i
standardnom devijacijom 7 cm, dok je u slučaju ženskih osoba to Gaussova distribucija sa srednjom vrijednošću 167 cm
i standardnom devijacijom 7 cm. Prikažite podatke te ih obojite plavom (1) odnosno crvenom bojom (0). Napišite
funkciju koja računa srednju vrijednost visine za muškarce odnosno žene (probajte izbjeći for petlju pomoću funkcije
np.dot). Prikažite i dobivene srednje vrijednosti na grafu.
'''

import numpy as np
import matplotlib.pyplot as plt
np.random.seed(42)

def get_gender_means(heights,genders):
    male_mean = np.dot(heights, genders) / (genders==1).sum()
    female_mean = np.dot(heights, np.where((genders == 0) | (genders == 1), genders ^ 1, genders)) / (genders==0).sum()
    return int(male_mean), int(female_mean)

sample_size=100
genders=np.random.randint(2, size=sample_size)

heights=[]
for gender in genders:
    if gender==1:
        heights.append(*np.random.normal(180, 7, 1))
    else:
        heights.append(*np.random.normal(167, 7, 1))

male_mean, female_mean= get_gender_means(heights, genders)

colors=['blue' if i==1 else 'red' for i in genders ]
plt.scatter(range(sample_size), heights, c=colors)
plt.figtext(.6, .8, "Male mean = {}".format(male_mean))
plt.figtext(.6, .7, "Female mean = {}".format(female_mean))
plt.show()

