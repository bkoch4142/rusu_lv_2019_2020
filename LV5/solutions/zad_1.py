from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as cluster
from sklearn.datasets import make_blobs


def generate_data(n_samples, flagc):

     if flagc == 1:
         random_state = 365
         X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
     elif flagc == 2:
         random_state = 148
         X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
         transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
         X = np.dot(X, transformation)

     elif flagc == 3:
         random_state = 148
         X, y = datasets.make_blobs(n_samples=n_samples,cluster_std=[1.0, 2.5, 0.5, 3.0], random_state=random_state)
     elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
     elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
     else:
        X = []

     return X


X = generate_data(500, 5)
x=X[:,0]
y=X[:,1]


n_clusters=3
kmeans = cluster.KMeans(n_clusters).fit(X)
labels=kmeans.labels_
centers=kmeans.cluster_centers_

colors=['g', 'b','y','m','c','w','k']
assert n_clusters <= len(colors)
label_color_map={i:colors[i] for i in range(n_clusters)}
label_color=[label_color_map[l] for l in labels]

plt.scatter(x,y, color=label_color)
plt.scatter(centers[:,0], centers[:,1], color='r')
plt.show()
