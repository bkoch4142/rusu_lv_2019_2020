from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as cluster
import pandas as pd
import random
from scipy.cluster.hierarchy import dendrogram, linkage


def generate_data(n_samples, flagc):

    if flagc == 1:
        random_state = 365
        X ,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
    elif flagc == 2:
        random_state = 148
        X ,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples ,cluster_std=[1.0, 2.5, 0.5, 3.0], random_state=random_state)
    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    else:
        X = []

    return X

X = generate_data(500, 5)

def measure_distance(x1,x2):
    return sum((x1-x2)**2)**0.5

def get_new_centroid(clusters, X):
    new_centroids = []
    new_df = pd.concat([pd.DataFrame(X), pd.DataFrame(clusters, columns=['cluster'])],
                      axis=1)
    for c in set(new_df['cluster']):
        current_cluster = new_df[new_df['cluster'] == c][new_df.columns[:-1]]
        cluster_mean = current_cluster.mean(axis=0)
        new_centroids.append(cluster_mean)
    return new_centroids

def get_assigned_centroid(ic, X):
    assigned_centroid = []
    for i in X:
        distance=[]
        for j in ic:
            distance.append(measure_distance(i, j))
        assigned_centroid.append(np.argmin(distance))
    return assigned_centroid

cluster_count=2
iter_count=10
centroids_idx=random.sample(range(0,len(X)),cluster_count)
centroids=[X[centroids_idx] for i in centroids_idx]
for i in range(iter_count):
    get_centroids = get_new_centroid(centroids, X)
    centroids = get_assigned_centroid(get_centroids, X)
    plt.figure()
    plt.scatter(np.array(centroids)[:, 0], np.array(centroids)[:, 1], color='black')
    plt.scatter(X[:, 0], X[:, 1], alpha=0.1)
    plt.show()

