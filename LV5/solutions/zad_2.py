from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as cluster

def generate_data(n_samples, flagc):

    if flagc == 1:
        random_state = 365
        X ,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
    elif flagc == 2:
        random_state = 148
        X ,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples ,cluster_std=[1.0, 2.5, 0.5, 3.0], random_state=random_state)
    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    else:
        X = []

    return X


X = generate_data(500, 5)
x=X[:,0]
y=X[:,1]

inertias=[]
for cluster_count in np.linspace(2,20,19):
    kmeans = cluster.KMeans(int(cluster_count)).fit(X)
    inertias.append(kmeans.inertia_)

plt.plot(np.linspace(2,20,19), inertias)
plt.show()